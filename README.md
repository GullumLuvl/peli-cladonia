Cladonia theme for Pelican static website generator
===================================================

Sober theme with minimal features:

- switches between dark and light mode with a button.
- Titles are khaki-green. In dark mode, the background is pure black, but the
  light mode background is slightly colored.
- Mathjax is included (from CDN).
- **No** analytics.

Can display additional information with some Pelican plugins:

- internal search with [pelican-search](https://github.com/pelican-plugins/search);
- similar articles with [pelican-similar-posts](https://github.com/pelican-plugins/similar-posts).

It started as a copy of [Peli-Kiera](https://github.com/aleylara/Peli-Kiera),
from which it inherits these features:

- float-right and float-left figure placement;
- display read times with plugin [readtime](https://github.com/getpelican/pelican-plugins/tree/master/readtime).

I developed it for my personal webpage: <http://www.normalesup.org/~glouvel>.

## Installation

Pelican is a static website generator in Python. See [Pelican-Docs](https://docs.getpelican.com).

1. Go to the root folder of your site and copy this repository inside a `themes` folder;
2. add the path in `pelicanconf.py`:

        THEME = 'themes/peli-cladonia'

3. if using the `search` plugin, install `stork` and indicate its location with
   the variable `STORK_DIR`.

## License

MIT License

