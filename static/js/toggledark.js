// At load time, set the class attribute to the dark value if user prefers dark.
window.onload = function() {
    // Load user choice from cookie if it was set.
    let userdarkmode = "" ;
    let cookie = decodeURIComponent(document.cookie);
    let cookiesplit = cookie.split(';');
    for (let i=0; i<cookiesplit.length; i++) {
        let c = cookiesplit[i];
        while (c[0] == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf('darkmode=') == 0) {
            userdarkmode = c.substring(9, c.length);
            break;
        }
    }
    if (userdarkmode == "dark") {
        document.body.classList.add("dark");
        document.body.classList.remove("light");
    } else if (userdarkmode == "light") {
        document.body.classList.add("light");
        document.body.classList.remove("dark");
    // if no cookie setting, detect the system preference
    } else if (window.matchMedia("(prefers-color-scheme: dark)").matches) {
        document.body.classList.add("dark");
    } else {
        document.body.classList.add("light");
    }
}
